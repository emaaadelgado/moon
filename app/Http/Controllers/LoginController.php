<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use App\Models\User;

class LoginController extends Controller
{
    public function all()
    {
        return response()->json(User::all());
    }

    public function create(Request $user)
    {
        $data = $user->validate([
            'name' => 'required',
            'last_name' => 'required',
            'dni' => 'required|unique:users,dni',
            'cellphone' => 'sometimes',
            'email' => ['unique:users,email', 'required', 'email'],
            'password' => 'required|string'
        ]);

        $data['password'] = Hash::make($data['password']);


        $user = User::create($data);

        $token = $user->createToken('moon')->accessToken;

        return response()->json(['message' => 'User created succesfully', 'user' => $data, 'token' => $token]);
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!Auth::attempt($data)) {
            return response()->json(['error' => __('auth.failed')], 401);
        }

        $user = Auth::user();

        $token = $user->createToken('moon')->accessToken;

        return response()->json(['user' => $user->email, 'message' => 'Welcome ' . $user->name . '!!', 'token' => $token]);
    }

    public function password_forgot(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email|exists:users'
        ]);

        $response = Password::sendResetLink($data);

        return response()->json([
            'message' => __($response)
        ]);
    }

    public function password_reset(Request $request)
    {

        $data = $request->validate([
            'email' => 'required|email|exists:users',
            'token' => 'required|string',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $response = Password::reset($data, function ($user, $password) {

            $user->password = Hash::make($password);
            $user->setRememberToken(Str::random(60));
            $user->save();

            //event(new PasswordReset($user));
        });

        return response()->json([
            'message' => __($response)
        ]);
    }

    public function update(Request $request)
    {
        if (!is_null(User::whereEmail($request->email)->first())) {
            $user_temp = User::whereEmail($request->email)->first();
            $data = $request->validate([
                'name' => 'sometimes',
                'last_name' => 'sometimes',
                'dni' => 'sometimes|unique:users,dni',
                'cellphone' => 'sometimes',
                'email' => ['exists:users,email', 'sometimes', 'email', Rule::unique('users', 'email')->ignore($user_temp->id)]
            ]);

            if (!is_null(User::whereEmail($request->email))) {
                $user = User::whereEmail($request->email);
                $user->update($data);
                $user = User::whereEmail($request->email)->get();
                return response()->json(['message' => 'User updated succesfully', 'user' => $user], 201);
            } else {
                return response()->json(['error' => 'Error, user cant be updated. Please verify the information'], 401);
            }
        } else {
            return response()->json(['error' => 'User not found. Please verify the information'], 404);
        }
    }
}
