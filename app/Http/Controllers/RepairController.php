<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Repair;
use Illuminate\Auth\Events\Validated;

class RepairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {

        return response()->json(Repair::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'client_id' => ['required', 'exists:clients,id'],
            'issue_description' => 'sometimes',
            'observations' => 'sometimes',
            'is_active' => 'required',
            'labour_price' => 'sometimes',
            'created_at' => 'required',
            'finished_at' => 'sometimes',
        ]);

        $repair = new Repair($data);

        $repair->save();

        return response()->json(['message' => 'Reparation created succesfully', 'repair' => $repair]);
    }

    /* Search a repair by Client */

    public function searchByClient(Request $request)
    {
        $client = $request->validate([
            'client' => ['exists:clients,id']
        ]);

        $client = $request->input('client');

        $repair = Repair::where('client_id', "=", $client)->get();

        return response()->json($repair);
    }

    /* Search a repair by id */
    public function find(Repair $repair)
    {
        return response()->json(Repair::find($repair->id));
    }

    /* Update a repair */
    public function update(Request $request, Repair $repair)
    {
        $data = $request->validate([
            'client_id' => ['sometimes', 'exists:clients,id'],
            'issue_description' => 'sometimes',
            'is_active' => 'sometimes',
            'labour_price' => 'sometimes',
            'finished_at' => 'sometimes'
        ]);
        $repair->update($data);

        return response()->json(['message' => 'Reparation updated succesfully', 'repair' => $repair]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Repair $repair)
    {
        $repair->delete();
        return response()->json(['message' => 'Reparation deleted succesfully', 'Reparation' => $repair]);
    }
}
