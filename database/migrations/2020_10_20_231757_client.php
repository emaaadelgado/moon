<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Client extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('last_name');
            $table->date('birthdate')->nullable()->date_format("d-m-Y");
            $table->string('dni')->unique();
            $table->string('email')->nullable()->unique();
            $table->string('cellphone');
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->date('last_repair')->nullable()->date_format("d-m-Y-H:i:s");
            $table->date('last_purchase')->nullable()->date_format("d-m-Y-H:i:s");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
