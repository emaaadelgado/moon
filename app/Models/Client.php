<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name',
        'last_name',
        'birthdate',
        'dni',
        'email',
        'cellphone',
        'address',
        'country',
        'city',
        'province',
        'last_repair',
        'last_purchase'
    ];
    use HasFactory;
}
