<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;


class CountryController extends Controller
{
    /* Show all the countries */

    public function all()
    {
        $countries = DB::table('countries as c')
            ->select('*')
            ->orderBy('c.name', 'asc')
            ->get();

        return response()->json($countries);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchByName(Request $request)
    {
        $name = $request->input('name');
        $countries = Country::where('name', 'LIKE', "%{$name}%")->orderBy('name', 'asc')->get();
        return response()->json($countries);
    }


    /* Create a new country */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'unique:countries,name']
        ]);

        $country = new Country($data);

        $country->save();
        return response()->json(['message' => 'Country created succesfully', 'country' => $country], 201);
    }

    /* Search a country by name */
    public function search(Request $request)
    {
        $name = $request->input('name');

        $countries = Country::where('name', 'LIKE', "%{$name}%")->get();

        return response()->json($countries);
    }

    /* Search a country by id */
    public function find(Country $country)
    {
        return response()->json($country);
    }

    /* Update a country */
    public function update(Request $request, Country $country)
    {
        $data = $request->validate([
            'name' => ['required', Rule::unique('country', 'name')->ignore($country->name)]
        ]);

        $country->update($data);
        return response()->json(['message' => 'Country updated succesfully', 'country' => $country]);
    }

    /* Delete a country */
    public function delete(Country $country)
    {
        $country->delete();
        return response()->json(['message' => 'Country deleted succesfully', 'country' => $country]);
    }
}
