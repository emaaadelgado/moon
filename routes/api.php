<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\RepairController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

////////////////* USERS */////////////////
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/clients', [ClientController::class, 'all']);
});




////////////////* CLIENTS */////////////////



/* Route::post('/clients/create', [ClientController::class, 'create']);

Route::get('/clients/{client}', [ClientController::class, 'find']);

Route::put('/clients/{client}',  [ClientController::class, 'update']);

Route::delete('/clients/{client}', [ClientController::class, 'delete']); */


////////////////* USERS */////////////////


/* Route::get('/users', [LoginController::class, 'all']);

Route::post('/user', [LoginController::class, 'login']);

Route::post('/user/create', [LoginController::class, 'create']);

Route::put('/user/update', [LoginController::class, 'update']);

Route::delete('/user/delete', [LoginController::class, 'delete']); */

/* Route::get('/admins', [AdminController::class, 'all']);

Route::post('/admins/create', [AdminController::class, 'create']);

Route::post('/admins/login', [AdminController::class, 'login']);

Route::post('/admins/logout', [AdminController::class, 'logout']);

Route::get('/admins/{admin}', [AdminController::class, 'find']);

Route::put('/admins/{admin}',  [AdminController::class, 'update']);

Route::delete('/admins/{admin}', [AdminController::class, 'delete']);
 */
////////////////* COUNTRY */////////////////

/* Route::get('/countries', [CountryController::class, 'all']);

Route::post('/countries/create', [CountryController::class, 'create']);

Route::get('/countries/search', [CountryController::class, 'search']);

Route::get('/countries/{country}', [CountryController::class, 'find']);

Route::put('/countries/{country}',  [CountryController::class, 'update']);

Route::delete('/countries/{country}', [CountryController::class, 'delete']); */

////////////////* STATE */////////////////

/* Route::get('/states', [StateController::class, 'all']);

Route::post('/states/create', [StateController::class, 'create']);

Route::get('/states/search', [StateController::class, 'search']);

Route::get('/states/{state}', [StateController::class, 'find']);

Route::put('states/{state}',  [StateController::class, 'update']);

Route::delete('/states/{state}', [StateController::class, 'delete']); */

////////////////* CITY */////////////////

/* Route::get('/cities', [CityController::class, 'all']);

Route::post('/cities/create', [CityController::class, 'create']);

Route::get('/cities/search', [CityController::class, 'search']);

Route::get('/cities/{city}', [CityController::class, 'find']);

Route::put('cities/{city}',  [CityController::class, 'update']);

Route::delete('/cities/{city}', [CityController::class, 'delete']); */

////////////////* REPAIR */////////////////

/* Route::get('/repairs', [RepairController::class, 'all']);

Route::post('/repairs/create', [RepairController::class, 'create']);

Route::get('/repairs/searchByClient', [RepairController::class, 'searchByClient']);

Route::get('/repairs/{repair}', [RepairController::class, 'find']);

Route::put('repairs/{repair}',  [RepairController::class, 'update']);

Route::delete('/repairs/{repair}', [RepairController::class, 'delete']);
 */