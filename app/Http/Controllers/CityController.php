<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\City;


class CityController extends Controller
{
    /* Show all the Cities */
    public function all()
    {
        return response()->json(City::all());
    }

    /* Create a new City */
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'unique:cities,name'],
            'state_id' => ['required', 'exists:states,id']
        ]);

        $city = new City($data);

        $city->save();

        return response()->json(['message' => 'City created succesfully', 'city' => $city]);
    }

    /* Search a city by name */
    public function search(Request $request)
    {
        $name = $request->input('name');

        $city = City::where('name', 'LIKE', "%{$name}%")->get();

        return response()->json($city);
    }

    /* Search a city by id */
    public function find(City $city)
    {
        return response()->json(City::find($city->id));
    }

    /* Update a city */
    public function update(Request $request, City $city)
    {
        $data = $request->validate([
            'name' => ['sometimes', Rule::unique('cities', 'name')->ignore($city->id)],
            'state_id' => ['sometimes', 'exists::states,id']
        ]);

        $city->update($data);
        return response()->json(['message' => 'City updated succesfully', 'city' => $city]);
    }

    /* Delete a city */
    public function delete(City $city)
    {
        $city->delete();
        return response()->json(['message' => 'City was deleted succesfully', 'city' => $city]);
    }
}
