<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Exists;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('clients.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'dni' => 'required|unique:clients,dni',
            'cellphone' => 'required',
            'email' => 'sometimes',
            'address' => 'sometimes',
            'country' => 'sometimes',
            'province' => 'sometimes',
            'city' => 'sometimes',
            'birthdate' => 'sometimes',
            'last_repair' => 'sometimes',
            'last_purchase' => 'sometimes'
        ]);

        $client = new Client($data);

        $client->save();
        return response()->json(['message' => 'Client created succesfully', 'client' => $client], 201);
    }

    public function searchDniClient(Request $request)
    {
        $dni = $request->input('searchDni');
        $user = Client::where('dni', '=', $dni)->first();
        if ($user === null) {
            return response()->json(false);
            // user doesn't exist
        } else return response()->json(true);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
