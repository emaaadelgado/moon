<?php

use App\Http\Controllers\CountryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'index'])->name('index');
Route::get('clients/searchDni', [UserController::class, 'searchDniClient']);
Route::get('clients/create', [UserController::class, 'create'])->name('clients.create');
Route::post('clients', [UserController::class, 'store'])->name('clients.store');
/* countries */
Route::get('/allCountries', [CountryController::class, 'all']);
Route::get('/country/search', [CountryController::class, 'searchByName']);
Route::post('country', [CountryController::class, 'store'])->name('country.store');
