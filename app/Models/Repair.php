<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{
    protected $fillable = [
        'client_id',
        'issue_description',
        'observations',
        'is_active',
        'labour_price',
        'created_at',
        'finished_at'
    ];
    public $timestamps = false;
    use HasFactory;
}
