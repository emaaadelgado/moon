<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductPriceRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_price_records', function (Blueprint $table) {
            $table->id('id')->unsigned();
            $table->foreignId('product_id')->constrained('products');
            $table->dateTime('from_date');
            $table->decimal('product_price')->references('price')->on('products');
            $table->integer('provider_id')->references('provider_id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_price_records');
    }
}
