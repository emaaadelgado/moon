<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use Illuminate\Validation\Rule;

class ClientController extends Controller
{
    /* Show all the clients */
    public function all()
    {
        return Client::all();
    }


    /* Create a new Client */
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'dni' => 'required|unique:clients,dni',
            'cellphone' => 'required',
            'address' => 'sometimes',
            'city' => 'sometimes',
            'province' => 'sometimes',
            'birthdate' => 'sometimes',
            'last_repair' => 'sometimes',
            'last_purchase' => 'sometimes'
        ]);

        $client = new Client($data);

        $client->save();
        return response()->json(['message' => 'Client created succesfully', 'client' => $client], 201);
    }


    /*  Find a client */
    public function find(Client $client)
    {

        return response()->json($client, 200);
    }

    /* Update a client */
    public function update(Request $request, Client $client)
    {
        $data = $request->validate([
            'name' => 'sometimes',
            'last_name' => 'sometimes',
            'dni' => ['sometimes', Rule::unique('clients', 'dni')->ignore($client->id)],
            'cellphone' => 'sometimes',
            'address' => 'sometimes',
            'id_city' => 'sometimes',
            'province' => 'sometimes',
            'birthdate' => 'sometimes',
            'last_repair' => 'sometimes',
            'last_purchase' => 'sometimes'
        ]);

        $client->update($data);

        return response()->json(['message' => 'Client updated succesfully', 'client' => $client], 200);
    }

    /* Delete a client */
    public function delete(Client $client)
    {
        $client->delete();
        return response()->json(['message' => 'Client deleted succesfully!']);
    }
}
