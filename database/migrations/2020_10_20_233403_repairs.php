<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Repairs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->foreignId('client_id')->constrained('clients');
            $table->text('issue_description')->nullable();
            $table->text('observations')->nullable();
            $table->boolean('is_active');
            $table->decimal('labour_price')->nullable();
            $table->date('created_at');
            $table->date('finished_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repairs');
    }
}
