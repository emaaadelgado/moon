<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return Admin::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'dni' => 'required|unique:admins,dni',
            'cellphone' => 'sometimes',
            'email' => 'required|unique:admins,email|email',
            'password' => 'required|min:8'
        ]);

        $data['password'] = Hash::make($data['password']);

        $admin = new Admin($data);

        $admin->save();
        return response()->json(['message' => 'Admin created succesfully', 'admin' => $admin], 201);
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required',
            'password' => 'required|min:8'
        ]);

        /*  if (!Auth::attempt($data)) {
            return response()->json(['error' => __('auth.failed')], 401);
        } */

        $admin = Auth::user();

        $token = $admin->createToken('moon')->accessToken;

        return response()->json(['user' => $admin->email, 'message' => 'Welcome ' . $admin->name . '!!', 'token' => $token]);
    }


    public function find(Admin $admin)
    {
        if (!$admin) {
            return response()->json('Admin not found', 404);
        }

        return response()->json($admin, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $admin = Admin::whereEmail($request->email)->first();
        /* If admin exists, update it */
        if (!is_null($admin)) {
            $data = $request->validate([
                'name' => 'sometimes',
                'last_name' => 'sometimes',
                'dni' => ['sometimes', Rule::unique('admins', 'dni')->ignore($admin->id)],
                'cellphone' => 'sometimes',
                'email' => ['sometimes', Rule::unique('admins', 'email')->ignore($admin->id)],
                'password' => 'sometimes'
            ]);
            /* If field password is not null, password is hashed and updated */
            if (!is_null($admin->password)) {
                $data['password'] = Hash::make($data['password']);
                $admin->update($data);
                return response()->json(['message' => 'Admin updated succesfully', 'Admin' => $admin]);
            }

            $admin->update($data);

            return response()->json(['message' => 'Admin was updated succesfully', 'admin' => $admin], 200);
        } else {

            return response()->json(['message' => 'Admin not found']);
        }
    }



    public function delete(Admin $admin)
    {
        $admin->delete();
        return response()->json(['message' => 'Admin was deleted succesfully', 'admin' => $admin], 200);
    }
}
