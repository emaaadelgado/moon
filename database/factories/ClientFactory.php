<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'cellphone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'birthdate' => $this->faker->date($format = 'Y-m-d', $max = '-18 years'),
            'dni' => $this->faker->ean13,
            'address' => $this->faker->streetAddress,
            'province' => $this->faker->country,
            'last_repair' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'last_purchase' => $this->faker->date($format = 'Y-m-d', $max = 'now')
        ];
    }
}
