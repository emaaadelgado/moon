<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id('id')->unsigned();
            $table->string('product_code')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->decimal('price');
            $table->boolean('has_attributes')->nullable();
            $table->boolean('is_active')->nullable();
            $table->foreignId('provider_id')->constrained('providers')->nullable();

            $table->timestamps(); //created_at update_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
