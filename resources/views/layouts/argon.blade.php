<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Motomecanica Lucas</title>

    <!-- Favicon -->
    <link rel="icon" href="/assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="/assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="/assets/css/argon.css" type="text/css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/assets/css/footer/footer.css" type="text/css">
    <link rel="stylesheet" href="/assets/css/modal/modal.css" type="text/css">
    {{-- Vue-Select CSS --}}
    <link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">
</head>

<body>
    <div id="app">
        <!-- Sidenav -->
        <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-ema-side"
            id="sidenav-main">
            <div class="scrollbar-inner">
                <!-- Brand -->
                <div class="sidenav-header  align-items-center border-bottom-ema">
                    <a class="" href="/">
                        <img src="/assets/img/logo/logolucas.png" width="120px" alt="logolucas">
                    </a>
                </div>
                <div class="navbar-inner ">
                    <!-- Collapse -->
                    <div class="collapse  navbar-collapse" id="sidenav-collapse-main">
                        <!-- Nav items -->
                        <ul class="navbar-nav">
                            {{-- ADMINISTRADORES --}}
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="collapse" href="#collapseExample" role="button"
                                    aria-expanded="false" aria-controls="collapseExample">
                                    <i class="ni ni-single-02 text-dark"></i>
                                    Clientes
                                </a>
                                <div class="collapse sidebar-collapse item" id="collapseExample">
                                    <a class="nav-link" href="{{ route('clients.create') }}">
                                        <i class="ni ni-fat-add text-dark"></i>
                                        <span class="nav-link-text">Crear</span>
                                    </a>
                                    <a class="nav-link" href="#">
                                        <i class="ni ni-archive-2 text-dark"></i>
                                        <span class="nav-link-text">Buscar</span>
                                    </a>
                                    <a class="nav-link" href="#">
                                        <i class="ni ni-ruler-pencil text-dark"></i>
                                        <span class="nav-link-text">Editar</span>
                                    </a>
                                    <a class="nav-link" href="#">
                                        <i class="ni ni-fat-remove text-dark"></i>
                                        <span class="nav-link-text">Eliminar</span>
                                    </a>
                                </div>
                            </li>

                            {{-- CAJA --}}
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i class="ni ni-tv-2 text-primary"></i>
                                    <span class="nav-link-text">Caja</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i class="ni ni-single-02 text-yellow"></i>
                                    <span class="nav-link-text">Proveedores</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i class="ni ni-bullet-list-67 text-default"></i>
                                    <span class="nav-link-text">Ordenes</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i class="ni ni-key-25 text-info"></i>
                                    <span class="nav-link-text">Productos</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i class="ni ni-pin-3 text-primary"></i>
                                    <span class="nav-link-text">Reparaciones</span>
                                </a>
                            </li>
                        </ul>
                        <!-- Divider -->
                        <hr class="my-3">
                    </div>
                </div>
            </div>
        </nav>
        <!-- Main content -->
        <div class="main-content bg-ema-side" id="panel">
            <!-- Topnav -->
            <nav class="navbar navbar-top navbar-expand navbar-dark bg-ema-nav border-bottom">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Navbar links -->
                        @include('include.navbar')
                    </div>
                </div>
            </nav>

            <!-- Header -->
            <div class="header bg-ema-header">
                @yield('header')
            </div>
            <!-- Page content -->
            <div class="container-fluid pt-5 ">
                @yield('content')
            </div>
            <div>
                <!-- Footer -->
                @yield('footer')
            </div>

        </div>
    </div>
    <!-- Custom JS -->
    <!-- include VueJS first -->
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs/dist/tf.min.js"></script>
    <!-- use the latest vue-select release -->
    <script src="https://unpkg.com/vue-select@3.0.0"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/assets/js/formValidation.js"></script>



    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <!-- Optional JS -->
    <script src="/assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="/assets/vendor/chart.js/dist/Chart.extension.js"></script>
    <!-- Argon JS -->
    <script src="/assets/js/argon.js?v=1.2.0"></script>

</body>

</html>
