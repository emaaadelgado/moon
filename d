[1mdiff --git a/app/Http/Controllers/CityController.php b/app/Http/Controllers/CityController.php[m
[1mindex 5a2a0e8..0dafb89 100644[m
[1m--- a/app/Http/Controllers/CityController.php[m
[1m+++ b/app/Http/Controllers/CityController.php[m
[36m@@ -38,6 +38,17 @@[m [mclass CityController extends Controller[m
         return response()->json(['message' => 'City created succesfully', 'city' => $city]);[m
     }[m
 [m
[32m+[m[32m    /* Search a city by name */[m
[32m+[m
[32m+[m[32m    public function search(Request $request)[m
[32m+[m[32m    {[m
[32m+[m[32m        $name = $request->input('name');[m
[32m+[m
[32m+[m[32m        $city = City::where('name', 'LIKE', "%{$name}%")->get();[m
[32m+[m
[32m+[m[32m        return response()->json($city);[m
[32m+[m[32m    }[m
[32m+[m
     /**[m
      * Display the specified resource.[m
      *[m
[36m@@ -49,14 +60,7 @@[m [mclass CityController extends Controller[m
         return response()->json(City::find($city->id));[m
     }[m
 [m
[31m-    public function search(Request $request)[m
[31m-    {[m
[31m-        $name = $request->input('name');[m
 [m
[31m-        $city = City::where('name', 'LIKE', "%{$name}%")->get();[m
[31m-[m
[31m-        return response()->json($city);[m
[31m-    }[m
 [m
 [m
     /**[m
[36m@@ -80,11 +84,12 @@[m [mclass CityController extends Controller[m
     /**[m
      * Remove the specified resource from storage.[m
      *[m
[31m-     * @param  int  $id[m
[32m+[m[32m     * @param  int  $city[m
      * @return \Illuminate\Http\Response[m
      */[m
[31m-    public function delete($id)[m
[32m+[m[32m    public function delete(City $city)[m
     {[m
[31m-        //[m
[32m+[m[32m        $city->delete();[m
[32m+[m[32m        return response()->json(['message' => 'City was deleted succesfully', 'city' => $city]);[m
     }[m
 }[m
