<?php

namespace App\Http\Controllers;

use App\Models\State;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class StateController extends Controller
{
    /* Show all the states */
    public function all()
    {
        return response()->json(State::all());
    }

    /* Create a state */
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'unique:states,name'],
            'country_id' => ['required', "exists:countries,id"]
        ]);

        $state = new State($data);

        $state->save();
        return response()->json(['message' => 'State created succesfully', 'state' => $state], 201);
    }


    /* Search State by name */
    public function search(Request $request)
    {
        $name_req = $request->input('name');

        $state = State::where('name', 'LIKE', "%{$name_req}%")->get();

        /*  $state = DB::table('states')->where('name', $request->name); */

        return response()->json($state);
    }

    /* Find a state if exists */
    public function find(State $state)
    {
        return response()->json(State::find($state->id));
    }

    /* Update a state */
    public function update(Request $request, State $state)
    {
        $data = $request->validate([
            'name' => ['sometimes', Rule::unique('states', 'name')->ignore($state->id)],
            'country_id' => ['sometimes', 'exists:countries,id']
        ]);
        $state->update($data);
        return response()->json(['message' => 'State was updated succesfully', 'state' => $state]);
    }

    /* Delete a state */
    public function delete(State $state)
    {
        $state->delete();
        return response()->json(['message' => 'State deleted succesfully', 'state' => $state]);
    }
}
