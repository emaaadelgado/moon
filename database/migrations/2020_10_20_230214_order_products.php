<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->foreignId('order_id')->constrained('orders');
            $table->bigInteger('product_id')->references('id')->on('products');
            $table->bigInteger('quantity');
            $table->decimal('unit_price');
            $table->decimal('total_amount');
            $table->integer('provider_id')->references('provider_id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
