@extends('layouts.argon')

{{-- HEADER --}}
@section('header')
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">INICIO</h6>
                </div>
            </div>
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total traffic</h5>
                                    <span class="h2 font-weight-bold mb-0">350,897</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                <span class="text-nowrap">Since last month</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                                    <span class="h2 font-weight-bold mb-0">2,356</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                        <i class="ni ni-chart-pie-35"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                <span class="text-nowrap">Since last month</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
                                    <span class="h2 font-weight-bold mb-0">924</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                        <i class="ni ni-money-coins"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                <span class="text-nowrap">Since last month</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                                    <span class="h2 font-weight-bold mb-0">49,65%</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                        <i class="ni ni-chart-bar-32"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                <span class="text-nowrap">Since last month</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{-- CONTENT CREATE CLIENTS --}}
@section('content')
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" validate action="{{ route('clients.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-12 col-md-6">
                        {{-- nombre --}}
                        <div class="form-group">
                            <label for="name" class="form-control-label">Nombre <span
                                    class="text-danger small">*</span></label>

                            <input type="text" name="name" class="form-control form-control-sm" placeholder="Nombre"
                                id="name-input" required>
                            <div class="valid-feedback">
                                Todo ok!
                            </div>
                            <div class="invalid-feedback">
                                Debes ingresar un nombre
                            </div>
                        </div>
                        {{-- apellido --}}
                        <div class="form-group">
                            <label for="lastname" class="form-control-label">Apellido <span
                                    class="text-danger small">*</span></label>
                            <input type="text" name="last_name" class="form-control form-control-sm" placeholder="Apellido"
                                id="lastname-input" required>
                            <div class="valid-feedback">
                                Todo ok!
                            </div>
                            <div class="invalid-feedback">
                                Debes ingresar un apellido
                            </div>
                        </div>
                        {{-- DNI --}}
                        <div class="form-group">
                            <label for="dni" class="form-control-label">N° Documento <span
                                    class="text-danger small">*</span></label>

                            <buscador-dni required />
                            <div class="valid-feedback">
                                Todo ok!
                            </div>
                            <div class="invalid-feedback">
                                Debes ingresar un numero de documento
                            </div>
                        </div>

                        {{-- email --}}
                        <div class="form-group">
                            <label for="email" class="form-control-label">Email </label>
                            <input class="form-control form-control-sm" name="email" type="email"
                                placeholder="email@email.com" id="email-input">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        {{-- fecha de nacimiento --}}
                        <div class="form-group">
                            <label for="birthdate" class="form-control-label">Fecha de Nacimiento <span
                                    class="text-danger small">*</span></label>
                            <input class="form-control form-control-sm" name="birthdate" type="date" placeholder=""
                                id="date-input" required>

                            <div class="valid-feedback">
                                Todo ok!
                            </div>
                            <div class="invalid-feedback">
                                Debes ingresar la fecha de nacimiento
                            </div>
                        </div>
                        {{-- telefono --}}
                        <div class="form-group">
                            <label for="cellphone" class="form-control-label">Telefono/Celular <span
                                    class="text-danger small">*</span></label>
                            <input class="form-control form-control-sm" name="cellphone" type="number" min="0"
                                placeholder="+54375812345" id="cellphone-input" required>
                            <div class="valid-feedback">
                                Todo ok!
                            </div>
                            <div class="invalid-feedback">
                                Debes ingresar un telefono/celular
                            </div>
                        </div>
                        {{-- Pais --}}
                        <div class="form-group">
                            <label for="country" class="form-control-label">País <span
                                    class="text-danger small">*</span></label>
                            <div class="row">
                                <buscador-pais />
                                <div class="valid-feedback">
                                    Todo ok!
                                </div>
                                <div class="invalid-feedback">
                                    Debes ingresar un Pais
                                </div>
                            </div>
                        </div>
                        {{-- direccion --}}
                        <div class="form-group">
                            <label for="address" class="form-control-label">Direccion <span
                                    class="text-danger small">*</span></label>
                            <input type="text" name="address" class="form-control form-control-sm" placeholder="Direccion"
                                id="address-input" required>
                            <div class="valid-feedback">
                                Todo ok!
                            </div>
                            <div class="invalid-feedback">
                                Debes ingresar una direccion
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <!-- Guardar Modal -->
                    <button class="btn btn-success" type="submit">Guardar</button>
                    <a class="btn btn-danger" data-toggle="modal" data-target="#cancel-modal">Cancelar</a>
                    {{-- --}}
                    <!-- Guardar Modal -->
                    <div class="modal fade" id="save-modal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Cliente creado
                                        correctamente!
                                    </h5>
                                </div>
                                <div class="modal-footer-ema">
                                    <div class="modal-option">
                                        <button type="submit" class="btn btn-success">Aceptar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Cancel Modal -->
                    <div class="modal fade" id="cancel-modal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">¿Seguro deseas
                                        cancelar?
                                    </h5>
                                </div>
                                <div class="modal-footer-ema">
                                    <div class="modal-option">
                                        <a class="btn btn-danger" href="{{ route('index') }}">Aceptar</a>
                                    </div>
                                    <div class="modal-option">
                                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-12">
                    <small class=""><span class="text-danger">*</span> Campos requeridos</small>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('footer')
    <div class="content"></div>
    <footer class="row footer-ema">
        <div class="copyright col-xs-12 col-sm-3 col-sm-pull-6">
            <p> &copy; 2020 - EmaDev </p>
            <p class="font-weight-bold ml-1"> Dueño: Lucas Krauchuk </p>
            <p class="font-weight-bold ml-1"> Motomecanica Lucas - Sistema de Gestión </p>
        </div><!-- Ende Copyright -->
        <div class="sozial col-xs-12 col-sm-6 col-sm-push-6">
            <ul class="media-ema">
                <li>
                    <a href="#">
                        <img class="logo"
                            src="https://cdn2.iconfinder.com/data/icons/black-white-social-media/32/online_social_media_facebook-128.png">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="logo"
                            src="https://cdn2.iconfinder.com/data/icons/black-white-social-media/32/twitter_online_social_media-128.png">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="logo"
                            src="https://cdn2.iconfinder.com/data/icons/black-white-social-media/32/instagram_online_social_media_photo-128.png">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="logo"
                            src="https://cdn2.iconfinder.com/data/icons/black-white-social-media/32/online_social_media_google_plus-128.png">
                    </a>
                </li>
            </ul>
        </div><!-- Ende Sozial media -->



    </footer>

@endsection
@push('script')
@endpush
