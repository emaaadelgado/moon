window._ = require('lodash');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/* DECLARANDO VUE DE MANERA GLOBAL */
window.Vue = require('vue');

/* TODOS LOS ARCHIVOS QUE TERMINEN EN .VUE SE CARGAN */
const files = require.context("./", true, /\.vue$/i);
files.keys().map((key) => {
  return Vue.component(
    key
      .split("/")
      .pop()
      .split(".")[0],
    files(key).default
  );
});

/* INSTANCIANDO VUE DE FORMA GLOBAL */
const app = new Vue({
    el: "#app",
    delimiters: ['<%', '%>']
  });


try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

