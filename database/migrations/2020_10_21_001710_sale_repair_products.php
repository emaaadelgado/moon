<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SaleRepairProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_repair_products', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->foreignId('repair_product_id')->constrained('repair_products');
            $table->decimal('unit_price');
            $table->decimal('total_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_repair_products');
    }
}
